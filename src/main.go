package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

type Link struct {
	Id    int
	Short string
	Full  string
}

var db *sql.DB

func GenerateString() string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890")
	new_string := make([]rune, 10)
	for i := range new_string {
		new_string[i] = letters[rand.Intn(len(letters))]
	}
	return string(new_string)
}

// request handlers
func LinkListHandler(w http.ResponseWriter, r *http.Request) {
	rows, err := db.Query("select * from links")
	if err != nil {
		log.Println(err)
	}

	links := []Link{}

	for rows.Next() {
		l := Link{}
		err := rows.Scan(&l.Id, &l.Short, &l.Full)
		if err != nil {
			log.Println(err)
			continue
		}
		links = append(links, l)
	}

	tmpl, _ := template.ParseFiles("templates/home.html")
	tmpl.Execute(w, links)
}

func LinkRedirectHandle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	short := "localhost:8000/link/" + vars["short"]
	log.Println(short)
	row := db.QueryRow("select * from links where short = ?", short)
	link := Link{}

	err := row.Scan(&link.Id, &link.Short, &link.Full)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(404), http.StatusNotFound)
	} else {
		http.Redirect(w, r, link.Full, 301)
	}
}

func DeleteLinkHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	_, err := db.Exec("delete from links where id=?", id)
	if err != nil {
		log.Println(err)
	}
	http.Redirect(w, r, "/", 301)
}

func ShortLinkCreateHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			log.Println(err)
		}

		full_link := r.FormValue("full")

		generated := false
		for generated != true {
			short_link := "localhost:8000/link/" + GenerateString()
			_, err := db.Exec("insert into links (short, full) values (?, ?)",
				short_link, full_link)
			if err != nil {
				continue
			}
			break
		}
		http.Redirect(w, r, "/", 301)
	} else {
		http.ServeFile(w, r, "templates/create.html")
	}
}

func main() {
	database, err := sql.Open("sqlite3", "store.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	db = database

	router := mux.NewRouter()
	router.HandleFunc("/", LinkListHandler)
	router.HandleFunc("/link/{short:[a-zA-Z0-9]+}", LinkRedirectHandle)
	router.HandleFunc("/delete_link/{id:[0-9]+}", DeleteLinkHandler)
	router.HandleFunc("/create_link/", ShortLinkCreateHandler)

	http.Handle("/", router)

	fmt.Println("Server is listening...")
	http.ListenAndServe(":8000", nil)
}
